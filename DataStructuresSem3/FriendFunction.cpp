
#include <iostream>

using namespace std;

class report;

class reg
{
	int roll;
	char name[20];
public:
	void read ()
	{
		cout << "\nEnter roll number : " ;
		cin >> roll ;
		cout << "\nEnter name : " ;
		cin >> name ;
	}
	int retroll()
	{
		return roll ;
	}
	friend void details ( report , reg );
} stu[10] ;

class report
{
	static int count ;
	char tname[10] ;
	int roll;
	int m1, m2, m3, min;
	float avg ;
public:
	void read( int m = 40 )
	{
		cout << "\nEnter test name : " ;
		cin >> tname ;
		cout << "\nEnter roll Number : " ;
		cin >> roll ;
		cout << "\nEnter three marks : " ;
		cin >> m1 >> m2 >> m3 ;
		avg = (float)(m1 + m2 + m3 ) / 3 ;
		min = m ;
	}
	int retroll()
	{
		return roll ;
	}
	friend void details ( report , reg );
} stud[10] ;

int report::count;

void details ( report ob1, reg ob2 )
{
	cout << "\nThe name is : " << ob2.name ;
	cout << "\nRoll Number : " << ob2.roll ;
	cout << "\nThe Marks in test " << ob1.tname << " are  : " ;
	cout << ob1.m1 << ", " << ob1.m2 << ", " << ob1.m3 << '.' ;
	cout << "\nThe average of the marks is : " << ob1.avg ;
	cout << "\nThe rank is " ;
	if ( ob1.avg < ob1.min )
		cout << "FAIL. " ;
	else 
		cout << "PASS. " ;
	++ob1.count ;
	cout << "\n\nThis function has been called " << ob1.count << " times in this program.\n " ;
}

int main()
{
	int op ;
	int s1 = 0, s2 = 0 ;
	int roll , t1 = -1, t2 = -1;
	menu:cout << "\nMain Menu\n1. Insert Student\n2. Insert Marks\n3. Display Details\n4. Exit\n\nEnter your option : " ; 
	cin >> op ;
	switch (op)
	{
	case 1:
		if ( s1>=10 )
		{
			cout << "\n!!!!Out of memory !!!!" ;
			break;
		}
		stu[s1].read() ;
		++s1 ;
		break ;
	case 2:
		if ( s2>=10 )
		{
			cout << "\n!!!!Out of memory !!!!" ;
			break;
		}
		stud[s2].read(50) ;
		++s2 ;
		break ;
	case 3:
		cout << "Enter the Roll Number you want to search for : " ;
		cin >> roll ;
		t1 = -1;
		t2 = -1 ;
		for ( int t = 0 ; t < 10 ; ++t )
		{
			if (  stu[t].retroll() == roll )
				t1 = t ;
			if (  stud[t].retroll() == roll )
				t2 = t ;
			if ( t1 != -1 && t2 != -1 )
				break ;
		}
		details(stud[t2],stu[t1]);
		break;
	case 4:
		goto end ;
	default:
		break;
	}
	goto menu ;
	end:return 0 ;
}