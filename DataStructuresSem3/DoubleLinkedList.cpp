
/* Double Linked List */

#include <stdio.h>
#include <malloc.h>

struct node
{
	int data;
	struct node *prev, *next ;
} *start, *end, *temp, *temp2 ;

void insert ( int data )
{
	int op, search ,flag ;
	temp = (struct node*)malloc(sizeof(struct node));
	temp->data = data ;
	printf ( "\nInsert Menu\n1. At first\n2. At Middle\n3. At Last\n" );
	printf ( "\tEnter your option : " );
	scanf_s ( "%d", &op );
	switch (op)
	{
	case 1:
		temp->next = start->next ;
		temp->next->prev = temp ;
		temp->prev = start ;
		start->next = temp;
		break;
	case 2:
		flag = 1 ;
		printf ( "\nEnter element you want to search : " );
		scanf_s ( "%d", &search );
		temp2 = start->next ;
		while (temp2 != end)
		{
			if ( temp2->data == search )
			{
				flag = 0 ;
				break;
			}
			temp2 = temp2->next ;
		}
		if ( flag == 1 )
			printf ( "\nNo such element was found." );
		else
		{
			temp->next = temp2->next ;
			temp->next->prev = temp ;
			temp->prev = temp2 ;
			temp2->next = temp ;
		}
		break ;
	case 3:
		end->prev->next = temp ;
		temp->prev = end->prev ;
		end->prev = temp ;
		temp->next = end ;
		break ;
	default:
		printf ( "\nInvalid option." );
	}
}

void Delete()
{
	int op, search ,flag ;
	printf ( "\nDelete Menu\n1. At first\n2. At Middle\n3. At Last\n" );
	printf ( "\tEnter your option : " );
	scanf_s ( "%d", &op );
	switch (op)
	{
	case 1:
		temp = start->next ;
		start->next = temp->next ;
		start->next->prev = start ;
		free (temp) ;
		break;
	case 2:
		flag = 1 ;
		printf ( "\nEnter element you want to search : " );
		scanf_s ( "%d", &search );
		temp = start->next ;
		while (temp != end)
		{
			if ( temp->data == search )
			{
				flag = 0 ;
				break;
			}
			temp = temp->next ;
		}
		if ( flag == 1 )
			printf ( "\nNo such element was found." );
		else
		{
			temp->prev->next = temp->next ;
			temp->next->prev = temp->prev ;
			free (temp) ;
		}
		break ;
	case 3:
		temp = end->prev ;
		end->prev = temp->prev ;
		temp->prev->next = end ;
		free (temp) ;
		break ;
	default:
		printf ( "\nInvalid option." );
	}
}

void display()
{
	printf ( "\nPrinting from beginning to end : " );
	temp = start->next ;
	while ( temp != end )
	{
		printf ( "%d\t", temp->data ) ;
		temp = temp->next ;
	}
	printf ( "\nPrinting from end to beginning : " );
	temp = end->prev ;
	while (temp != start )
	{
		printf ( "%d\t", temp->data ) ;
		temp = temp->prev ;
	}
}

void initialize ()
{
	start = (struct node*)malloc(sizeof(struct node));
	start->data = 0 ;
	start ->prev = NULL ;
	end = (struct node*)malloc(sizeof(struct node));
	end->data = 0 ;
	end->next = NULL ;
	start->next = end ;
	end->prev = start ;
}

int main()
{
	int op, data ;
	initialize();
menu:printf ( "\n\nMain Menu\n1. Insert Data\n2. Delete Data\n3. Display List\n 4. Exit\nEnter your option: " );
	scanf_s ( "%d", &op ) ;
	switch (op)
	{
	case 1:
		printf ( "\nEnter data to be inserted: " );
		scanf_s ( "%d", &data ) ;
		insert(data) ;
		break ;
	case 2:
		Delete () ;
		break ;
	case 3:
		display() ;
		break ;
	case 4:
		goto end ;
	default :
		printf ( "\nIvalid Input. " );
	}
	goto menu ;
end:return 0 ;
}