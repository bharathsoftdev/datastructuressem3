DataStructuresSem3
==================

Data Structures Semester ( Anna University )

This is Read Me file.

Hi readers, This repositroy contains all the Data Structures related C programs written by me during the third semester of my B.E. CSE under-graduate degree. I have created this repository so that anyone who needs them can make use of them and learn from them. I request all of you just NOT to copy the programs but instead use them as a reference. As with all GitHub free repositroy, this is under open source license. So everyone is absolutely free to use them.

I hope you all find this useful. I will constantly keep adding new programs as I code them. I also request you to point out any bugs ( if you find any ). I will try my best to rectify them. If you want me to add any program to this repository, mail them to me. My mail ID is s4bharath@hotmail.com

Eat. Sleep. Code.

Bye.
